# Getting Started with Create React App
 
1. To run this project on local clone using " git clone https://mateen_kadwaikar@bitbucket.org/mateen_kadwaikar/grid-stack-high-chart.git"

2. Once clone, use "npm install" command to install all the packages. Use 'npm start ' to run the application.

# Libraries used

1. HighCharts - For showing charts 
2. GridStack

# Current Implementation

1. Resizable feature from GridStack
2. Line and Bar Chart from HighCharts
3. The draggable feature from GridStack

# Remaining
1. 