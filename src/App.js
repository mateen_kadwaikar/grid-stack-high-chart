import React, { useEffect, useState } from 'react';
import { GridStack } from 'gridstack';
import BarChart from './BarChart/BarChart';
import LineChart from './LineChart/LineChart';
import PieChart from './PieChart/PieChart';
import "./App.css";
import 'gridstack/dist/h5/gridstack-dd-native';
import 'gridstack/dist/gridstack.css';


const App = () => {

  const [resizeValue, setResize] = useState(null);
  const [divHeight, setHeight] = useState(0);
  useEffect(() => {
    var grid = GridStack.init({
      resizable: {
        autoHide: true,
        handles: 'e,se,s,sw,w'
      },
      draggable: {
        handles: '.grid-stack-item-content',
        scroll: false,
        appendTo: 'body',
        containment: null
      },
      float: true,
      placeholderText: 'Grid',
      // cellHeight: "10%",
      margin: 10,
    });
    console.log(grid);
    grid.on('change', (event, items) => {
      console.log(event);
      console.log(items);
      setHeight(items[0]._rect.h);
      setResize(event);
    });
  }, []);

  return (
    <div className="display-flex">
      <div className='flex-left'>
        Left Side Section
      </div>

      <div className='flex-right'>
        Rigt Side Section
        <div className="grid-stack">
          <div className="grid-stack-item border-dark" gs-w="5" gs-h='3' gs-x="0" gs-y="0">
            <div className="grid-stack-item-content" data-line='lineChart'>
              <LineChart chartResize={resizeValue}/></div>
          </div>
          <div className="grid-stack-item border-dark" gs-w="5" gs-h='3' gs-x="6" gs-y="0">
            <div className="grid-stack-item-content" ><BarChart  chartResize={resizeValue} /></div>
          </div>
          <div className="grid-stack-item border-dark" gs-w="5" gs-h='3' gs-x="0" gs-y="4">
            <div className="grid-stack-item-content"><PieChart  chartResize={resizeValue}/></div>
          </div>
          <div className="grid-stack-item border-dark" gs-w="10" gs-h='3' gs-x="0" gs-y="8">
            <div className="grid-stack-item-content"><PieChart /></div>
          </div> 
        </div>
      </div>
    </div>
  );
}

export default App;
